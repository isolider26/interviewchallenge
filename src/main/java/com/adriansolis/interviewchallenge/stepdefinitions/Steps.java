package com.adriansolis.interviewchallenge.stepdefinitions;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Steps {

    static WebDriver driver;
    static String path = "src/main/resources/webdriver/chromedriver.exe";

    @Before
    public void beforeScenario() {
        System.out.println("Start the browser");
        System.setProperty("webdriver.chrome.driver", path);
        driver = new ChromeDriver();
    }

    @Given("I navigate to a webPage")
    public void navigateToWebPage(){
        System.out.println("Navigate");
        driver.get("https://amazon.com");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Given("This is a test case")
    public  void testCase() throws Throwable {
        System.out.println("TEST!!!");
    }

    @After
    public void afterScenario() {
        driver.close();
    }
}
